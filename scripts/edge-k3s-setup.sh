#!/bin/sh
# Copyright (c) 2020, Arm Ltd
# Author: Luis E. P.
#
# This script will setup a k3s master on this machine

# This is the k3s version to be downloaded and installed
export K3S_VERSION="v1.18.3+k3s1"

# Log for installation. Useful for debugging
export INSTALLATION_LOG="logs/edge-k3s-setup-"$(date +"%Y%m%d%H%M%S").log

# Create logs directory if doesn't exist...
mkdir -p logs

# Starting logging bracket...
{

# We need to set the advertise IP to the IP other nodes can use to reach the k3s master
# Depending on where the k3s master is running, the advertise IP is the same as the internal IP or
# it is a different, external IP. The latter case is common when running in cloud machines.
#
# More info on how to get the IP:
# https://stackoverflow.com/questions/23362887/can-you-get-external-ip-address-from-within-a-google-compute-vm-instance
# 
# cloud-init is a helpful tool to determine which way we detect external address

echo "\n=========================================================================\n"
echo "Trying to determine if this machine is running in the cloud by using cloud-init"
type cloud-init
if [ $? -eq 0 ]
then
    echo "cloud-init is available!"

    echo "Getting platform info from cloud-init"
    export PLATFORM=$(cloud-init query platform)

    if [ $? -ne 0 ]
    then
        echo "cloud-init failed, exiting script..."
        exit -1
    fi

    case ${PLATFORM} in
        ec2)
            # Use this export if AWS EC2
            echo "Running on AWS EC2"
            export ADVERTISE_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4) || (echo "Not able to get IP address from AWS";exit -1)
            ;;

        gce)
            # Use this export if Google GCE
            echo "Running on Google GCE"
            export ADVERTISE_IP=$(curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip)
            ;;

            # Use this export if Azure
            # export ADVERTISE_IP=$(curl -H Metadata:true "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2017-08-01&format=text")

        *) 
            # Use this export if external IP equals internal IP
            echo "'${PLATFORM} unknown, assuming local server"
            export ADVERTISE_IP=$(hostname -I | awk '{print $1;}')
            ;;

    esac
else
    echo "cloud-init not available, assuming local server"
    export ADVERTISE_IP=$(hostname -I | awk '{print $1;}')
fi

echo "Using $ADVERTISE_IP as the k3s master advertise address..."
echo "Please confirm this is the IP the nodes can use to reach this k3s master!"
echo "\n=========================================================================\n"

# Store the advertise address in a file for later use
echo $ADVERTISE_IP > server_ip

# Run k3s-uninstall if it exists
echo "\nRemoving k3s if currently installed...\n"
[ -x /usr/local/bin/k3s-uninstall.sh ] && /usr/local/bin/k3s-uninstall.sh

# Store the k3s version in a file for later use
echo $K3S_VERSION > k3s_version

echo "\n=========================================================================\n"
echo "Installing k3s $K3S_VERSION using the k3s install.sh script..."
echo "\n=========================================================================\n"
# TODO: the "--disable-agent" flag is supported, but discouraged. Find and test a new way to make master unschedulable. Taints?
curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=$K3S_VERSION sh -s - \
     --disable-agent --no-deploy servicelb --no-deploy traefik --no-deploy metrics-server --no-flannel --no-deploy coredns --advertise-address $ADVERTISE_IP --cluster-dns 169.254.0.2 --tls-san $ADVERTISE_IP

curl -sfl https://gitlab.com/arm-research/smarter/k3s/-/raw/master/package/smarter-image/remove-password-deleted-nodes.sh 
sudo mv remove-password-deleted-nodes.sh /usr/local/bin
sudo chmod a+x /usr/local/bin/remove-password-deleted-nodes.sh

sudo cat << EOF > /etc/systemd/system/k3s-node-password.service
[Unit]
Description=Lightweight Kubernetes add-on
Wants=k3s.service

[Install]
WantedBy=multi-user.target

[Service]
Type=simple
KillMode=process
Delegate=yes
Restart=always
RestartSec=5s
ExecStart=/usr/local/bin/remove-password-deleted-nodes.sh
EOF

sudo systemctl enable k3s-node-password.service

echo "\n=========================================================================\n"
echo "Copying the kubeconfig to $PWD/edge-kube.config..."
sudo cat /etc/rancher/k3s/k3s.yaml > edge-kube.config

echo "Updating the address in edge-kube.conifg to the advertise address..."
sed -i "s/127.0.0.1/$ADVERTISE_IP/g" edge-kube.config

echo "Copying the node-token to $PWD/token..."
sudo cat /var/lib/rancher/k3s/server/node-token > token

echo "\n=========================================================================\n"
echo "kubectl cluster-info"
export KUBECONFIG=$PWD/edge-kube.config
kubectl cluster-info --kubeconfig $PWD/edge-kube.config

echo "\n=========================================================================\n"
echo "NOTE: If using kubectl from this machine, make sure to run 'export KUBECONFIG=$PWD/edge-kube.config'"
echo "IMPORTANT: Make sure to copy the important files (server_ip, token, k3s_version and edge-kube.config) to your dev machine for the next step"
echo "\n=========================================================================\n"

} 2>&1 | tee ${INSTALLATION_LOG}
# Ending debugging bracket...

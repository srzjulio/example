apiVersion: v1
kind: ConfigMap
metadata:
  name: fluent-bit-config
  namespace: default
  labels:
    k8s-app: fluent-bit
data:
  # Configuration files: server, input, filters and output
  # ======================================================
  fluent-bit.conf: |
    [SERVICE]
        Flush         1
        Log_Level     ${FLB_LOG_LVL}
        Daemon        off
        Parsers_File  parsers.conf
        HTTP_Server   Off
        HTTP_Listen   0.0.0.0
        HTTP_Port     2020
        Streams_File  mqtt-split.conf

    @INCLUDE netdata-input.conf
    @INCLUDE input-kubernetes.conf
    @INCLUDE input-mqtt.conf
    @INCLUDE filter-kubernetes.conf
    @INCLUDE filter-mqtt.conf
    @INCLUDE output-elasticsearch.conf
    @INCLUDE output-fluentd.conf

  netdata-input.conf: |
    [INPUT]
        Name        tcp
        Tag         netdata
        Listen      0.0.0.0
        Port        5170
        Chunk_Size  64
        Buffer_Size 128
        Format      json

  input-kubernetes.conf: |
    [INPUT]
        Name              tail
        Tag               kube.*
        Path              /var/log/containers/*.log
        Parser            docker
        DB                /var/log/flb_kube.db
        Mem_Buf_Limit     5MB
        Skip_Long_Lines   On
        Refresh_Interval  10

  input-mqtt.conf: |
    [INPUT]
        Name   mqtt
        alias  mqttdata
        Tag    mqttdata
        Listen 0.0.0.0
        Port   1883

  filter-kubernetes.conf: |
    [FILTER]
        Name                kubernetes
        Match               kube.*
        Kube_URL            https://kubernetes.default.svc:${K8S_EDGEMASTER_PORT}
        Kube_CA_File        /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        Kube_Token_File     /var/run/secrets/kubernetes.io/serviceaccount/token
        Kube_Tag_Prefix     kube.var.log.containers.
        Merge_Log           On
        Merge_Log_Key       log_processed
        K8S-Logging.Parser  On
        K8S-Logging.Exclude Off

  filter-mqtt.conf: |
    [FILTER]
        Name          record_modifier
        Match         car_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         person_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         bus_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         bicycle_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         motorcycle_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         sound_class
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         pms7003_count
        Record        hostname ${MY_NODE_NAME}

    [FILTER]
        Name          record_modifier
        Match         weatherbit_count
        Record        hostname ${MY_NODE_NAME}

  mqtt-split.conf: |
    [STREAM_TASK]
        Name  car_count
        Exec  CREATE STREAM car_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/car_count';

    [STREAM_TASK]
        Name  person_count
        Exec  CREATE STREAM person_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/person_count';

    [STREAM_TASK]
        Name  bus_count
        Exec  CREATE STREAM bus_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/bus_count';

    [STREAM_TASK]
        Name  bicycle_count
        Exec  CREATE STREAM bicycle_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/bicycle_count';

    [STREAM_TASK]
        Name  motorcycle_count
        Exec  CREATE STREAM motorcycle_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/motorcycle_count';

    [STREAM_TASK]
        Name  sound_class
        Exec  CREATE STREAM sound_class AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/sound_class';

    [STREAM_TASK]
        Name  pms7003_count
        Exec  CREATE STREAM pms7003_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/pms7003';
 
    [STREAM_TASK]
        Name  weatherbit_count
        Exec  CREATE STREAM weatherbit_count AS SELECT * FROM STREAM:mqttdata WHERE topic = '/demo/weatherbit';

  output-elasticsearch.conf: |
    [OUTPUT]
        Name            forward
        Match           kube.*
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off

  output-fluentd.conf: |
    [OUTPUT]
        Name            forward
        Match           *_count
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
    [OUTPUT]
        Name            forward
        Match           *_class
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off
    [OUTPUT]
        Name            forward
        Match           netdata
        Host            ${FLUENTD_HOST}
        Port            ${FLUENTD_PORT}
        Shared_Key      ${FLUENTD_PASSWORD}
        Self_Hostname   ${MY_NODE_NAME}
        tls             on
        tls.verify      off

  parsers.conf: |
    [PARSER]
        Name   json
        Format json
        Time_Key time
        Time_Format %d/%b/%Y:%H:%M:%S %z

    [PARSER]
        Name        docker
        Format      json
        Time_Key    time
        Time_Format %Y-%m-%dT%H:%M:%S.%L
        Time_Keep   On
        # Command      |  Decoder | Field | Optional Action
        # =============|==================|=================
        Decode_Field_As   escaped    log

    [PARSER]
        Name        syslog
        Format      regex
        Regex       ^\<(?<pri>[0-9]+)\>(?<time>[^ ]* {1,2}[^ ]* [^ ]*) (?<host>[^ ]*) (?<ident>[a-zA-Z0-9_\/\.\-]*)(?:\[(?<pid>[0-9]+)\])?(?:[^\:]*\:)? *(?<message>.*)$
        Time_Key    time
        Time_Format %b %d %H:%M:%S

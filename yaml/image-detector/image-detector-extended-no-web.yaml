apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: image-detector-jetson
  labels:
    k8s-app: image-detector-jetson
spec:
  selector:
    matchLabels:
      name: image-detector-jetson
  template:
    metadata:
      labels:
        name: image-detector-jetson
    spec:
      nodeSelector:
        image-detector: "enabled"
        smarter.nodetype: "jetson"
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: image-detector
      initContainers:
      - name: init-pulse
        image: busybox:1.28
        command: ['sh', '-c', 'until nslookup gstreamer; do echo waiting for gstreamer; sleep 2; done;']
      containers:
      - name: image-detector
        imagePullPolicy: IfNotPresent
        command: ["python3"]
        args: ["demo.py", "-s", "1", "-p", "--detect-car", "--detect-person"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/image-detector:v2.4.3-arm64
        env:
        - name: MY_NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: MQTT_BROKER_HOST
          value: "fluent-bit"
        - name: LOG_LEVEL
          value: "DEBUG"
        - name: CAPTURE_STRING
          value: "rtspsrc location=rtsp://gstreamer:8554/video.h264.1 ! decodebin ! videoconvert ! appsink max-buffers=1 drop=true"
        - name: MODEL_NAME
          value: "ssd_mobilenet_coco"
        - name: TRITON_URL
          value: "triton:8001"
        - name: PROTOCOL
          value: "gRPC"
        resources:
          limits:
            memory: 200M
          requests:
            memory: 200M
      terminationGracePeriodSeconds: 30

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: image-detector
  labels:
    k8s-app: image-detector
spec:
  selector:
    matchLabels:
      name: image-detector
  template:
    metadata:
      labels:
        name: image-detector
    spec:
      nodeSelector:
        image-detector: "enabled"
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: smarter.nodetype
                operator: NotIn
                values:
                - jetson
      hostname: image-detector
      initContainers:
      - name: init-pulse
        image: busybox:1.28
        command: ['sh', '-c', 'until nslookup gstreamer; do echo waiting for gstreamer; sleep 2; done;']
      containers:
      - name: image-detector
        imagePullPolicy: IfNotPresent
        command: ["python3"]
        args: ["demo.py", "-s", "1", "-p", "--detect-car", "--detect-person", "--armnn"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/image-detector:v2.4.3-arm64
        env:
        - name: MY_NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: MQTT_BROKER_HOST
          value: "fluent-bit"
        - name: LOG_LEVEL
          value: "DEBUG"
        - name: CAPTURE_STRING
          value: "rtspsrc location=rtsp://gstreamer:8554/video.h264.1 ! decodebin ! videoconvert ! appsink max-buffers=1 drop=true"
        - name: MODEL_NAME
          value: "ssd_mobilenet_coco"
        - name: TRITON_URL
          value: "triton:8001"
        - name: PROTOCOL
          value: "gRPC"
        ports:
        - containerPort: 8080
        resources:
          limits:
            memory: 200M
          requests:
            memory: 200M
      terminationGracePeriodSeconds: 30

---

apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: image-detector-ec2
  labels:
    k8s-app: image-detector-ec2
spec:
  selector:
    matchLabels:
      name: image-detector-ec2
  template:
    metadata:
      labels:
        name: image-detector-ec2
    spec:
      nodeSelector:
        image-detector: "enabled"
        smarter.nodetype: "virtual"
      tolerations:
      - key: "smarter.type"
        value: "edge"
        effect: "NoSchedule"
      hostname: image-detector
      containers:
      - name: image-detector
        imagePullPolicy: IfNotPresent
        command: ["python3"]
        args: ["demo.py", "-s", "0", "-p", "--detect-car", "--detect-person", "--armnn"]
        image: registry.gitlab.com/arm-research/smarter/edge-workloads/image-detector:v2.4.3-arm64
        env:
        - name: MY_NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: MQTT_BROKER_HOST
          value: "fluent-bit"
        - name: LOG_LEVEL
          value: "DEBUG"
        - name: CAPTURE_STRING
          value: "http://210.148.114.53/-wvhttp-01-/GetOneShot?image_size=640x480&frame_count=1000000000"
        - name: MODEL_NAME
          value: "ssd_mobilenet_coco"
        - name: TRITON_URL
          value: "triton:8001"
        - name: PROTOCOL
          value: "gRPC"
        ports:
        - containerPort: 8080
        resources:
          limits:
            memory: 200M
          requests:
            memory: 200M
      terminationGracePeriodSeconds: 30

image:
  repository: quay.io/fluentd_elasticsearch/fluentd
  tag: v2.9.0
  pullPolicy: IfNotPresent
  

plugins:
  enabled: true
  pluginsList: ["fluent-plugin-influxdb", "fluent-plugin-record-modifier", "fluent-plugin-rewrite-tag-filter", "fluent-plugin-prometheus-smarter"]

service:
  annotations: {}
  type: NodePort
  nodePort: 30224
  ports:
    - name: "input-forward"
      protocol: TCP
      containerPort: 24224

metrics:
  enabled: true
  service:
    port: 24231
  serviceMonitor:
    enabled: true

annotations: 
  prometheus.io/scrape: "true"
  prometheus.io/port: "24231"

extraEnvVars:
  - name: ELASTICSEARCH_PASS
    valueFrom:
      secretKeyRef:
        name: elastic-credentials
        key: password
  - name: MY_NODE_NAME
    valueFrom:
      fieldRef:
        fieldPath: spec.nodeName
  - name: FLUENTD_PASSWORD
    valueFrom:
      secretKeyRef:
        name: fluentd-credentials
        key: password

extraVolumes:
  - name: fluentd-tls-cert
    secret:
      defaultMode: 420
      secretName: fluentd-tls-cert

extraVolumeMounts:
  - name: fluentd-tls-cert
    mountPath: /etc/td-agent/certs
    readOnly: true

output:
  host: elasticsearch-master.default.svc.cluster.local
  port: 9200
  scheme: http
  sslVersion: TLSv1
  buffer_chunk_limit: 2M
  buffer_queue_limit: 8

configMaps:
  general.conf: |
    # Prevent fluentd from handling records containing its own logs. Otherwise
    # it can lead to an infinite loop, when error in sending one message generates
    # another message which also fails to be sent and so on.
    <match fluentd.**>
      @type null
    </match>

    # Used for health checking
    <source>
      @type http
      port 9880
      bind 0.0.0.0
    </source>

  system.conf: |-
    <system>
      root_dir /tmp/fluentd-buffers/
    </system>
  forward-input.conf: |
    <source>
      @type forward
      port 24224
      bind 0.0.0.0
      <transport tls>
        min_version TLSv1_1
        max_version TLSv1_2
        ciphers ALL:!aNULL:!eNULL:!SSLv2
        insecure false

        cert_path /etc/td-agent/certs/ca.crt
        private_key_path /etc/td-agent/certs/tls.key
      </transport>
      <security>
        self_hostname "#{ENV['MY_NODE_NAME']}"
        shared_key "#{ENV['FLUENTD_PASSWORD']}"
      </security>
    </source>
  filter-input.conf: |
    <filter netdata>
      @type record_modifier
      # remove "labels" key from record as influxdb doesn't like brackets
      remove_keys labels,chart_name,prefix
    </filter>
  output.conf: |
    <match kube.**>
      @id elasticsearch
      @type elasticsearch
      @log_level info
      include_tag_key true
      host "#{ENV['OUTPUT_HOST']}"
      port "#{ENV['OUTPUT_PORT']}"
      user elastic
      password "#{ENV['ELASTICSEARCH_PASS']}"
      scheme "#{ENV['OUTPUT_SCHEME']}"
      ssl_version "#{ENV['OUTPUT_SSL_VERSION']}"
      logstash_format true
      <buffer>
        @type file
        path /var/log/fluentd-buffers/kubernetes.system.buffer
        flush_mode interval
        retry_type exponential_backoff
        flush_thread_count 2
        flush_interval 5s
        retry_forever
        retry_max_interval 30
        chunk_limit_size "#{ENV['OUTPUT_BUFFER_CHUNK_LIMIT']}"
        queue_limit_length "#{ENV['OUTPUT_BUFFER_QUEUE_LIMIT']}"
        overflow_action block
      </buffer>
    </match>

    <match *_count>
      @type influxdb
      @id object-count-influxdb
      host  influxdb
      port  8086
      dbname waggledemo
      sequence_tag _seq
      tag_keys ["hostname", "topic"]
    </match>

    <match *_class>
      @type influxdb
      @id sound-class-influxdb
      host  influxdb
      port  8086
      dbname waggledemo
      sequence_tag _seq
      tag_keys ["hostname", "topic"]
    </match>

    # Create new tags based on labels in netdata based on average format
    # Info on formatting is found here: https://learn.netdata.cloud/docs/agent/backends/prometheus
    <match netdata>
      @type rewrite_tag_filter
      <rule>
        key chart_context
        pattern ^(.*)\.(.*)$
        tag temp.netdata_$1_$2
      </rule>
    </match>

    <match temp.netdata**>
      @type rewrite_tag_filter
      remove_tag_prefix temp
      <rule>
        key units
        pattern ([a-zA-Z_:][a-zA-Z0-9_:]*)\/([a-zA-Z0-9_:]*)
        tag ${tag}_$1_per$2_average
      </rule>
      <rule>
        key units
        pattern ^[a-zA-Z_:][a-zA-Z0-9_:]*$
        tag ${tag}_$1_average
      </rule>
    </match>

    <match *_average>
      @type prometheus_metric
      key value
    </match>
